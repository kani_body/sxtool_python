# -*- coding: utf-8 -*-

import socket
import struct
import configparser

class Sx:
    _command  = [0xFB,0x80,
                0x80,0x00,
                0xFF,0x7B,
                0xFE,0x00,
                0x11,0x00,
                0x00,0x00,
                0x00,0x00,
                0x06,0x00,#command
                0x00,0x01,
                0x0C,0x00,#number of data area byte
                0x01,0x00,
                0x00,0x04,#Input:0x00,0x04 / Output:0x01,0x04 ArrayNo[22,23]
                0x01,0x00,#word position/ArrayNo[24]
                0x00,0x0F,#bit positon / ArrayNo[27] 
                0x01,0x00,#bit ON/OFF /ArrayNo[28]
                0x00,0x00]
    
    def __init__(self):
        self.iowords_r = {} # dictionary of  word position : address 
        self.iowords = {} # dictionary of address : word position
        self.iodata = {} #dictionary of address : data
        self.connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #connection for sx

    def get_data(self,address):
        ad = self.get_unique_address(address)

        if not ("X" in ad) :
            data = self.iodata[ad]
            return data
        
        bit = int(ad.split(".")[ad.count(".")])
        ad = ad[:ad.rindex(".")].replace("X","W")
        data = 1 if self.iodata[ad] & (2**bit) else 0
            
        return data

    def get_data_in_response(self,response): #メモリ読み出しコマンドの応答から読み出しメモリの種別,先頭アドレス,データを取り出す
        
        return (int.from_bytes(response[20:21],"little"),
                int.from_bytes(response[21:24],"little"),
                tuple(int.from_bytes(i,"little") for i in zip(response[26::2],response[27::2])))
    
    def __get_word_position(self,conf,codes,module,node,ad_num,ad_dic): #get_io用のメソッド
        word_num = 0
        ls =[]
        if node == []:
            ad_body = codes
        else:
            ad_body = ".".join([i[2] for i in node]) + "." +codes

        module_words = 0
        for i in [int(j) for j in module[3::2]]:
            module_words += i
        
        for io in module[2:]:
                #IW,QWの識別子を記憶
                if not(io.isdigit()):
                        tag = io.replace("OW","QW")
                        continue
                for word in range(0,int(io)):
                        ad = "%" + tag + ad_body + "." + str(word_num)        
                        word_num += 1
                        ad_dic[ad] = ad_num
                        if conf == "Config" and module_words == 1:
                            ad_num += 2
                        else:
                            ad_num += 1
        return ad_num 
    
    def get_io(self,ioarea):
        #引数のioareaには読み込むioarea.iniのパスを入力

        config = configparser.ConfigParser()
        config.read(ioarea)

        ad_num = 0 #現在処理中のアドレスが何ワード目かの情報を保存

        #探索木の初期値
        node = []
        conf = "Config"
        target = 1 #Config.nの何行目かを指す(0始まり)
        data = {}
        while target != -1:
            
            if not conf in data:
                data[conf] = [i for i in config[conf].items()] #一度作ったリストは辞書で管理してリスト化処理を最低限にする。要素は(局番:モジュール名)の組み合わせ
                
            module = data[conf][target][1].split()
            length = len(data[conf])
            
            if  not data[conf][target][0].isdigit() or not("@LNK" in module or "@IO" in module): #局番(数字)でないものと@LNK,@IOでないものは飛ばす
                if target+1 >= length:
                    conf,target,ad = [conf,-2,""] if len(node) == 0 else node.pop()
                target +=1
                continue
            
            if "@LNK" in module:
                node.append([conf,target,data[conf][target][0]])
                conf = module[2]
                target =0

            if "@IO" in module:
                ad_num = self.__get_word_position(conf,data[conf][target][0],module,node,ad_num,self.iowords)
                if target+1 >= length:
                    conf,target,ad = [conf,-2,""] if len(node) == 0 else node.pop()
                target +=1
        #ワード位置からの逆引き辞書作成
        for i in self.iowords.items():
            self.iowords_r[i[1]] = i[0]
        
    def get_unique_address(self,address): #%IX1.00.01.06⇒%IX1.0.1.6のようにするメソッド
        ad = [int(i) if i.isdigit() else i for i in address.split(".")] #数値は全て整数型へ変換して0埋めを除去

        for i , j in enumerate(ad[0]):
            if j.isdigit():
                ad[0] = ad[0][:i] + str(int(ad[0][i:])) #識別子に付随する数値から0埋めを除去して識別子+数値を文字列にして返す
                break

        ad = map(str,ad) #全て文字列型へ変換

        return ".".join(ad)#ピリオドで連結して返す


    def get_connection(self,ip,port):
        #相手と接続
        self.connection.connect((ip, port))

    def close_connection(self):
        self.connection.close()

    def io_read(self,start_ad,word_num):#読込先頭のIOワード位置と読出しワード数を指定 
        read_cmd = [0xFB,0x80,0x80,0x00,
                    0xFF,0x7B,0xFE,0x00,
                    0x11,0x00,0x00,0x00,
                    0x00,0x00,0x00,0x00,
                    0x00,0x01,0x06,0x00,
                    0x00,0x00,0x00,0x00,
                    0x00,0x00]
        
        read_cmd[21] = int(start_ad.to_bytes(3,"little")[0])
        read_cmd[22] = int(start_ad.to_bytes(3,"little")[1])
        read_cmd[23] = int(start_ad.to_bytes(3,"little")[2])
        read_cmd[24] = word_num.to_bytes(2,"little")[0]
        read_cmd[25] = word_num.to_bytes(2,"little")[1]
        #バイナリコードへ変換
        print(word_num)
        code_length = "!"+str(len(read_cmd))+"B"
        code = struct.Struct(code_length)
        packed_data = code.pack(*read_cmd)
        #コード変換したデータを送信
        self.connection.send(packed_data)
        #応答待ち
        res = self.connection.recv(1024)
        #応答からデータ抽出
        res_data = self.get_data_in_response(res)
        #読込データを格納
        for i,data in enumerate(res_data[2]):
            try :
                adrs = self.iowords_r[i+res_data[1]]
                self.iodata[adrs] = data
            except:
                continue
        
            
    def io_bit_write(self,address,data):#書込みたいアドレスとデータを指定(ON:1,OFF：0)
        write_cmd = [0xFB,0x80,0x80,0x00,
                    0xFF,0x7B,0xFE,0x00,
                    0x11,0x00,0x00,0x00,
                    0x00,0x00,0x06,0x00,
                    0x00,0x01,0x0C,0x00,
                    0x01,0x00,0x00,0x04,
                    0x00,0x00,0x00,0x00,
                    0x00,0x00,0x00,0x00]
        adrs = self.get_unique_address(address)
        if bool("IX" in adrs):
            io_kind = 0x00
        elif bool("QX" in adrs):
            io_kind = 0x01
        else:
            pass
        word_adrs = adrs.replace("X","W")
        word_adrs = word_adrs.split(".")
        bit_pos  = int(word_adrs.pop(-1))
        word_adrs = ".".join(word_adrs)
        word_pos = self.iowords[word_adrs]
        #bit_pos = int(adrs.split(".")[len(adrs.split("."))-1])
        if not(data == 0 or data == 1):
            data = 0
        #送信コマンド編集
        write_cmd[22] = io_kind
        write_cmd[24] = int(word_pos.to_bytes(3,"little")[0])
        write_cmd[25] = int(word_pos.to_bytes(3,"little")[1])
        write_cmd[26] = int(word_pos.to_bytes(3,"little")[2])
        write_cmd[27] = bit_pos
        write_cmd[28] = data
        
        #バイナリコードへ変換
        code_length = "!"+str(len(write_cmd))+"B"
        code = struct.Struct(code_length)
        packed_data = code.pack(*write_cmd)
        #コード変換したデータを送信
        self.connection.send(packed_data)
