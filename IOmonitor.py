# -*- coding: utf-8 -*-
import sxtool
import csv
import os
import sys
import tkinter as tk
import tkinter.filedialog
import configparser

PORT = 507

#---------Button用のcallback関数定義--------
def write_command(button,sx,address):
    def write_cmd():
        if button['bg'] == 'limegreen':
            sx.io_bit_write(address,1)
        elif button['bg'] == 'red':
            sx.io_bit_write(address,0)
    return write_cmd
#--------------------------------------------


#----------メイン画面の設定-------------------------------------
root = tk.Tk()
#PCの横と縦のサイズを取得
scr_w,scr_h = root.winfo_screenwidth(),root.winfo_screenheight()
#起動時の画面はディスプレイの横半分,縦max
root.geometry(str(scr_w//2)+'x'+str(scr_h))
#画面引き伸ばしの設定
root.grid_rowconfigure(0, weight = 1)
root.grid_columnconfigure(0, weight = 1)
#---------------------------------------------------------------


#----------------------FileDialogからmwt選択----------------------------------------------------------
ProjectPath = tkinter.filedialog.askopenfilename(initialdir = './',filetypes = (('mwt files','*.mwt'),))
#-----------------------------------------------------------------------------------------------------


#--------------------設定読込------------------------------------------------------
config = configparser.ConfigParser()
configfile= os.path.dirname(os.path.abspath(sys.argv[0]))+'/iomonitor.conf'
config.read(configfile)
ConfigTitle = config['Window']['title']
#ファイルダイアログでmwtファイルを選択しなかった場合、configファイルの設定を読み込む
if ProjectPath == "":
    ProjectPath = config['Project']['Path']
ioareapath = ProjectPath.replace('.mwt','/C/C_SX/ioarea.ini')
ConfigCsvFile = os.path.dirname(os.path.abspath(sys.argv[0]))+"/"+config['Project']['csvfile']
ConfigIp = config['Project']['ip']
ConfigButtonWidth = int(config['Config']['buttonwidth'])
ConfigButtonHeight = int(config['Config']['buttonheight'])
ConfigFontSize = int(config['Config']['fontsize'])
if config['Config']['fontbold'] == "True":
    ConfigFontBold = "bold"
else:
    ConfigFontBold = ""
ConfigMaxRow = int(config['Config']['maxrow'])
if ProjectPath != "":
    config.set('Project', 'Path', ProjectPath)
    config.write(open(configfile, 'w'))
#--------------------------------------------------------------------------------------


#----------------windowのtitleを設定値に変更-------------------------------------------
root.title(ConfigTitle)
#--------------------------------------------------------------------------------------


#----------------キャンバスを引き伸ばしありで配置-------------------------------------
cvs = tk.Canvas(root)
cvs.grid(row = 0,column = 0,sticky=(tk.N, tk.S, tk.E, tk.W))
#-------------------------------------------------------------------------------------


#-------------スクロールバー作成とキャンバスとの関連付け-------------------------------
scrollY = tk.Scrollbar(root,orient=tk.VERTICAL,command=cvs.yview)
scrollY.grid(row=0, column=1, sticky=tk.N+tk.S)
scrollX = tk.Scrollbar(root, orient=tk.HORIZONTAL,command=cvs.xview)
scrollX.grid(row=1, column=0,sticky=tk.E+tk.W)
cvs['xscrollcommand'] = scrollX.set
cvs['yscrollcommand'] = scrollY.set
#--------------------------------------------------------------------------------------

#------------マウスホイール操作に画面スクロールをバインド----------------------------------------
root.bind('<MouseWheel>',lambda event:cvs.yview_scroll(-1*(1 if event.delta>0 else -1),tk.UNITS))
#------------------------------------------------------------------------------------------------


#-------------ボタン配置用のフレーム作成------------------------
frame = tk.Frame(cvs)
cvs.create_window(0,0,window=frame,anchor=tk.NW)
#---------------------------------------------------------------


#---------SXとの接続-------------------
sx = sxtool.Sx()
sx.get_io(ioareapath)
sx.get_connection(ConfigIp,PORT)
word_num = max(sx.iowords.values())+1
sx.io_read(0,word_num)
#--------------------------------------


#-----------csvファイルから情報を読出してボタン生成-----------------------------------------
with open(ConfigCsvFile,"r") as f:
    lines = f.readlines()

ls = []
ad_ls = []
for i ,line in enumerate(lines):
    line = line.replace("\n","")
    txt = line.split(",")[0]
    adrs = line.split(",")[1]
    ad_ls.append(adrs)
    button = tk.Button(frame,text= txt,bg = "limegreen",width=ConfigButtonWidth,height=ConfigButtonHeight,font=("",ConfigFontSize,ConfigFontBold))
    button.configure(command = write_command(button,sx,adrs))
    button.grid(row = i % ConfigMaxRow,column = i//ConfigMaxRow)
    ls.append(button)
#--------------------------------------------------------------------------------------------


#---------画面更新してスクロールバーのスクロール範囲をButton配置してるFrameの大きさに設定------
root.update()
cvs['scrollregion']= (0,0,frame.winfo_width(),frame.winfo_height())
#----------------------------------------------------------------------------------------------


#----------------mainloop----------------
while True:
    sx.io_read(0,word_num)
    for i,ad in enumerate(ad_ls):
        if sx.get_data(ad) >= 1:
            ls[i]["bg"] = "red"
        else:
            ls[i]["bg"] = "limegreen"
    root.update_idletasks()
    root.update()
sx.close_connection()
#----------------------------------------
